import random
import time

import requests
import names
from loguru import logger

from web_constants import ADD_POST_PATH
from web_constants import LOGIN_PATH
from web_constants import ANALYTICS_PATH
from web_constants import USER_ACTIVITY_PATH

config = {
    "host_address": "http://127.0.0.1:5000",
    "number_of_users": 2,
    "max_posts_per_user": 2,
    "max_likes_per_user": 2,
    "analytics_date_from": "2022-03-21 00:34:10",
    "analytics_date_to": "2022-03-22 00:34:10"
}


def main():
    host_address = config['host_address']
    number_of_users = config['number_of_users']
    max_posts_per_user = config['max_posts_per_user']
    max_likes_per_user = config['max_likes_per_user']

    expected_number_of_likes = 0

    for user_index in range(number_of_users):
        # random_name = names.get_full_name()
        # email = f"{random_name}@fake.com"
        # password = "0cdaa742b9d41fa94b1c206b356c8c91"
        # username = f"{random_name}-kitty"

        random_name = "Dmitriy"
        email = "Dmitriy@fake.com"
        password = "hello"
        username = "Dmitriy"

        number_of_content = random.randrange(max_posts_per_user)
        number_of_likes = random.randrange(max_likes_per_user)

        expected_number_of_likes += number_of_likes

        random_content = {"title": f"My name is {random_name}",
                          "content": f"My friends call me {username}",
                          "slug": f"{random_name}-{username}"}

        # user_data = {'name': random_name,
        #             'email': email,
        #             'password_hash': password,
        #             'password_hash2': password,
        #             'username': username,
        #             'submit': 'submit'}

        # try:
        #    response = requests.post(f"{host_address}{ADD_USER_PATH}", data=user_data)
        #    print('response: ', response)
        #    logger.info(f'Пользователь по имени {random_name} успешно создан')

        # except Exception as exc:
        #    logger.error(f'Пользователь по имени {random_name} не создан: {exc}')
        #    continue

        requests.post(f"{host_address}{LOGIN_PATH}, data={random_content}")
        for post_index in range(number_of_content):
            try:
                requests.get(f"{host_address}{ADD_POST_PATH}, data={random_content}")
                logger.info(f'Пользователь по имени {random_name} успешно создал пост')

            except Exception as exc:
                logger.error(f'Пользователь по имени {random_name} не создал пост: {exc}')
                continue
            time.sleep(2)

        for like_index in range(number_of_likes):
            try:
                requests.get(f"{host_address}{ADD_POST_PATH}, data={random_content}")
                logger.info(f'Пользователь по имени {random_name} лайкнул пост')

            except Exception as exc:
                logger.error(f'Пользователь по имени {random_name} не лайкнул пост: {exc}')
                continue
            time.sleep(2)

        try:
            params = {'identifier': 1}
            activity_response = requests.get(f"{host_address}{USER_ACTIVITY_PATH}",
                                             params=params).json()

            logger.info(f'Активность пользователя по имени = {random_name}: '
                        f'последний раз логинился = {activity_response["date_last_login"]},'
                        f'Последняя активность = {activity_response["date_last_active"]}')

        except Exception as exc:
            logger.error(f'Пользователь по имени {random_name} не получил статистику активности: {exc}')
            continue
        time.sleep(2)

    try:
        params = {'date_from': config['analytics_date_from'],
                  'date_to': config['analytics_date_from']}

        likes_number = requests.get(f"{host_address}{ANALYTICS_PATH}",
                                    params=params).json()

        logger.info(f'Аналитика лайков: '
                    f'Ожидаемое количество лайков = {expected_number_of_likes},'
                    f'Выполненное количество лайков = {likes_number}')

    except Exception as exc:
        logger.error(f'Статистика лайков не получена: {exc}')


if __name__ == "__main__":
    main()
