from datetime import datetime

from flask import Flask
from flask import render_template
from flask import flash
from flask import redirect
from flask import url_for
from flask import jsonify
from flask import request
from flask_login import login_user
from flask_login import current_user
from flask_login import login_required
from flask_login import logout_user
from loguru import logger
from werkzeug.security import check_password_hash
from werkzeug.security import generate_password_hash

from webforms import LoginForm
from webforms import PostForm
from webforms import UserForm

from web_constants import PAGE_404
from web_constants import PAGE_500
from web_constants import POST_EDIT_PAGE
from web_constants import POSTS_PAGE
from web_constants import POST_PAGE
from web_constants import INDEX_PAGE
from web_constants import DASHBOARD_PAGE
from web_constants import ADD_USER_PAGE
from web_constants import ADD_POST_PAGE
from web_constants import UPDATE_PAGE
from web_constants import LOGIN_PAGE

from web_constants import DELETE_USER_PATH
from web_constants import LOGOUT_PATH
from web_constants import DASHBOARD_PATH
from web_constants import DELETE_POST_PATH
from web_constants import LOGIN_PATH
from web_constants import UPDATE_USER_PATH
from web_constants import POSTS_PATH
from web_constants import EDIT_POST_PATH
from web_constants import POST_PATH
from web_constants import ADD_POST_PATH
from web_constants import ADD_USER_PATH
from web_constants import MAIN_PATH
from web_constants import POST_LIKE_PATH
from web_constants import ANALYTICS_PATH
from web_constants import USER_ACTIVITY_PATH


class ApplicationConstructor:
    def __init__(self):
        self._app = Flask(__name__)

    @property
    def app(self):
        return self._app

    @staticmethod
    def define_login_managers(user_model, login_manager):
        @login_manager.user_loader
        def load_user(user_id):
            return user_model.query.get(int(user_id))

    def define_configurations(self, path_to_database, secret_key):
        self._app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{path_to_database}'
        self._app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        self._app.config['SECRET_KEY'] = secret_key

    def define_error_handlers(self):
        @self._app.errorhandler(404)
        def page_not_found(e):
            return render_template(PAGE_404), 404

        @self._app.errorhandler(500)
        def error_on_the_page(e):
            return render_template(PAGE_500), 500

    # TODO: Вынести дублирующийся код с datetime в отдельный метод
    def define_login_required_routers(self, database, user_model, user_post, postlike):

        @self._app.route(ANALYTICS_PATH, methods=['GET'])
        @login_required
        def analytics():
            args = request.args
            date_from = args['date_from']
            date_to = args['date_to']

            posts = user_post.query.filter(user_post.date_posted <= date_to).filter(user_post.date_posted >= date_from)

            response = {'likes_number': len(posts.likes)}
            return jsonify(response), 201

        @self._app.route(USER_ACTIVITY_PATH, methods=['GET'])
        @login_required
        def user_activity(identifier):
            user_info = user_model.query.get_or_404(identifier)

            response = {'user_name': user_info['username'],
                        'date_last_login': user_info['date_last_login'],
                        'date_last_active': user_info['date_last_active']}

            return jsonify(response), 201

        @self._app.route(POST_LIKE_PATH)
        @login_required
        def like_action(identifier, action):
            post = user_post.query.filter_by(id=identifier).first_or_404()
            if action == 'like':
                like = postlike(user_id=current_user.id,
                                post_id=post.id)
                database.session.add(like)

                logger.info(f'Number of likes user = {len(post.likes)}')

            if action == 'unlike':
                postlike.query.filter_by(user_id=current_user.id,
                                         post_id=post.id).delete()

                logger.info(f'Number of likes user = {len(post.likes)}')

            user_to_update = user_model.query.get_or_404(current_user.id)
            user_to_update.date_last_active = datetime.now()
            database.session.commit()

            posts = user_post.query.order_by(user_post.date_posted)
            return render_template(POSTS_PAGE, posts=posts)

        @self._app.route(EDIT_POST_PATH, methods=['GET', 'POST'])
        @login_required
        def edit_post(identifier):
            post = user_post.query.get_or_404(identifier)
            post_form = PostForm()
            if current_user.id != post.post_id:
                flash("You aren't authorized to edit this post")
                posts = user_post.query.order_by(user_post.date_posted)
                return render_template(POSTS_PAGE, posts=posts)

            if post_form.validate_on_submit():
                post.title = post_form.title.data
                post.slug = post_form.slug.data
                post.content = post_form.content.data

                user_to_update = user_model.query.get_or_404(current_user.id)
                user_to_update.date_last_active = datetime.now()

                database.session.add(post)
                database.session.commit()

                flash(f"Post is successfully updated")
                logger.info(f"Post with id={identifier} is successfully updated")

                return redirect(url_for('post', identifier=post.id))

            else:
                flash(f"Invalid data")
                logger.info(f"Invalid data for post with id={identifier}")
                return render_template(POST_EDIT_PAGE, form=post_form)

        @self._app.route(LOGOUT_PATH, methods=['GET', 'POST'])
        @login_required
        def logout():
            identifier = current_user.id
            logout_user()
            flash("Successfully logged out")
            logger.info(f"User with id={identifier} successfully logged out")
            return redirect(url_for('login'))

        @self._app.route(ADD_POST_PATH, methods=['GET', 'POST'])
        @login_required
        def add_post():
            # TODO: Убрать flash при первом запуске.

            post_form = PostForm()
            if post_form.validate_on_submit():
                database.session.add(user_post(title=post_form.title.data,
                                               content=post_form.content.data,
                                               post_id=current_user.id,
                                               slug=post_form.slug.data))

                user_to_update = user_model.query.get_or_404(current_user.id)
                user_to_update.date_last_active = datetime.now()

                database.session.commit()

                post_form.title.data = ''
                post_form.content.data = ''
                post_form.slug.data = ''

                flash("Post is successfully submitted")
                logger.info("Post is successfully submitted")

            else:
                flash("Post is not submitted. Invalid data !")
                logger.error("Post is not submitted. Invalid data")

            return render_template(ADD_POST_PAGE, form=post_form)

        @self._app.route(DELETE_POST_PATH)
        @login_required
        def delete_post(identifier):
            post_to_delete = user_post.query.get_or_404(identifier)
            if current_user.id != post_to_delete.post.id:
                flash("You aren't authorized to delete this post")
                posts = user_post.query.order_by(user_post.date_posted)
                return render_template(POSTS_PAGE, posts=posts)

            try:
                user_to_update = user_model.query.get_or_404(current_user.id)
                user_to_update.date_last_active = datetime.now()

                database.session.delete(post_to_delete)
                database.session.commit()

                flash(f"Post is successfully deleted")
                logger.info(f"Post with id={identifier} is successfully deleted")

                posts = user_post.query.order_by(user_post.date_posted)

            except Exception as exc:
                flash(f"Post is not deleted")
                logger.error(f"Error. Post with id={identifier} is not deleted: {exc}")

                posts = user_post.query.order_by(user_post.date_posted)

            return render_template(POSTS_PAGE, posts=posts)

        @self._app.route(DASHBOARD_PATH, methods=['GET', 'POST'])
        @login_required
        def dashboard():
            form = UserForm()
            name_to_update = user_model.query.get_or_404(current_user.id)
            if request.method != "POST":
                user_to_update = user_model.query.get_or_404(current_user.id)
                user_to_update.date_last_active = datetime.now()
                database.session.commit()

                return render_template(DASHBOARD_PAGE,
                                       form=form,
                                       name_to_update=name_to_update,
                                       id=current_user.id)

            name_to_update.name = request.form['name']
            name_to_update.email = request.form['email']
            name_to_update.username = request.form['username']
            name_to_update.about_author = request.form['about_author']
            name_to_update.date_last_active = datetime.now()

            database.session.commit()
            flash("User is successfully updated")
            logger.info("User is successfully updated")
            return render_template(DASHBOARD_PAGE,
                                   form=form,
                                   name_to_update=name_to_update)

        @self._app.route(UPDATE_USER_PATH, methods=['GET', 'POST'])
        @login_required
        def update(identifier):
            form = UserForm()
            name_to_update = user_model.query.get_or_404(identifier)
            if request.method != "POST":
                return render_template(UPDATE_PAGE,
                                       form=form,
                                       name_to_update=name_to_update,
                                       id=identifier)

            name_to_update.name = request.form['name']
            name_to_update.email = request.form['email']
            name_to_update.username = request.form['username']
            name_to_update.date_last_active = datetime.now()

            try:
                database.session.commit()
                flash(f"User is successfully updated")
                logger.info(f"User with id={identifier} is successfully updated")
                return render_template(UPDATE_PAGE,
                                       form=form,
                                       name_to_update=name_to_update, id=identifier)
            except Exception as exc:
                flash(f"User is not updated. Try again !")
                logger.error(f"User with id={identifier} is not updated: {exc}")
                return render_template(UPDATE_PAGE,
                                       form=form,
                                       name_to_update=name_to_update,
                                       id=identifier)

        @self._app.route(DELETE_USER_PATH)
        @login_required
        def delete(identifier):
            if identifier != current_user.id:
                flash("You are not allowed to delete this user's profile")
                return redirect(url_for('dashboard'))

            user_to_delete = user_model.query.get_or_404(identifier)
            user_form = UserForm()

            try:
                database.session.delete(user_to_delete)
                database.session.commit()
                flash(f"User is successfully deleted")
                logger.info(f"User with id={identifier} is successfully deleted")

                our_users = user_model.query.order_by(user_model.date_added)
                return render_template(ADD_USER_PAGE,
                                       form=user_form,
                                       name=user_form.name.data,
                                       our_users=our_users)

            except Exception as exc:
                flash(f"User is not deleted. Try again !")
                logger.error(f'User with id={identifier} is not deleted: {exc}')
                our_users = user_model.query.order_by(user_model.date_added)
                return render_template(ADD_USER_PAGE,
                                       form=user_form,
                                       name=user_form.name.data,
                                       our_users=our_users)

        @self._app.route(POSTS_PATH)
        @login_required
        def posts():
            user_to_update = user_model.query.get_or_404(current_user.id)
            user_to_update.date_last_active = datetime.now()
            database.session.commit()

            return render_template(POSTS_PAGE,
                                   posts=user_post.query.order_by(user_post.date_posted))

    def define_routers(self, database, user_model, user_post):
        @self._app.route(ADD_USER_PATH, methods=['GET', 'POST'])
        def add_user():
            user_form = UserForm()
            name = None

            if user_form.validate_on_submit():
                user = user_model.query.filter_by(email=user_form.email.data).first()
                if user is None:
                    hashed_pw = generate_password_hash(user_form.password_hash.data, "sha256")

                    current_time = datetime.now()

                    user = user_model(username=user_form.username.data,
                                      name=user_form.name.data,
                                      email=user_form.email.data,
                                      password_hash=hashed_pw,
                                      date_added=current_time,
                                      date_last_login=current_time,
                                      date_last_active=current_time)
                    try:
                        database.session.add(user)
                        database.session.commit()
                        flash("User is successfully added")
                        logger.info("User is successfully added")

                    except:
                        flash(f"Username is already taken")
                        logger.info(f"Username={user_form.username.data} is already taken")

                else:
                    flash(f"User is already exists")
                    logger.info(f"User with username={user_form.username.data} is already exists")

                user_form.name.data = ''
                user_form.username.data = ''
                user_form.email.data = ''
                user_form.password_hash.data = ''

            else:
                # TODO: Добавить flash о том что пароли не совпали
                # TODO: Вылетает ошибка если username уже занято

                flash("User is not added")
                logger.error("User is not added")

            our_users = user_model.query.order_by(user_model.date_added)
            return render_template(ADD_USER_PAGE,
                                   form=user_form,
                                   name=name,
                                   our_users=our_users)

        @self._app.route(LOGIN_PATH, methods=['GET', 'POST'])
        def login():
            user_form = LoginForm()
            user_data = user_model.query.filter_by(username=user_form.username.data).first()

            if not user_form.validate_on_submit():
                flash("Data is not valid")
                logger.error('Data is not valid')
                return render_template(LOGIN_PAGE, form=user_form)

            elif not user_data:
                flash("Try again. User doesn't exist!")
                logger.error('User doesnt exist!')
                return render_template(LOGIN_PAGE, form=user_form)

            elif not check_password_hash(user_data.password_hash, user_form.password.data):
                flash("Try again. Password is incorrect")
                logger.error('Password is incorrect')
                return render_template(LOGIN_PAGE, form=user_form)

            else:
                login_user(user_data)
                flash("Logged in successfully")
                logger.info("Logged in successfully")

                now = datetime.now()
                user_to_update = user_model.query.get_or_404(current_user.id)
                user_to_update.date_last_login = now
                user_to_update.date_last_active = now
                database.session.commit()

                return redirect(url_for('dashboard'))

        @self._app.route(POST_PATH)
        def post(identifier):

            user_to_update = user_model.query.get_or_404(current_user.id)
            user_to_update.date_last_active = datetime.now()
            database.session.commit()

            return render_template(POST_PAGE,
                                   post=user_post.query.get_or_404(identifier))

        @self._app.route(MAIN_PATH)
        def index():
            stuff = "Demo version of Social Network"
            return render_template(INDEX_PAGE, stuff=stuff)
