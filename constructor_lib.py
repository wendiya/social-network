from datetime import datetime

from flask_login import UserMixin
from werkzeug.security import check_password_hash
from werkzeug.security import generate_password_hash


def create_user_base(db):
    class Users(db.Model, UserMixin):
        id = db.Column(db.Integer, primary_key=True)
        username = db.Column(db.String(20), nullable=False, unique=True)
        name = db.Column(db.String(200), nullable=False)
        email = db.Column(db.String(120), nullable=False, unique=True)
        about_author = db.Column(db.Text(), nullable=True)
        date_added = db.Column(db.DateTime, default=datetime.utcnow)
        date_last_login = db.Column(db.DateTime, default=datetime.utcnow)
        date_last_active = db.Column(db.DateTime, default=datetime.utcnow)
        password_hash = db.Column(db.String(128))
        posts = db.relationship('Posts', backref='post')
        liked = db.relationship('PostLike', backref='user')

        @property
        def password(self):
            raise AttributeError('password is not a readable attribute!')

        @password.setter
        def password(self, password):
            self.password_hash = generate_password_hash(password)

        def verify_password(self, password):
            return check_password_hash(self.password_hash, password)

        def __repr__(self):
            return '<Name %r>' % self.name

    return Users


def create_post_like(db):
    class PostLike(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
        post_id = db.Column(db.Integer, db.ForeignKey('posts.id'))
        data_liked = db.Column(db.DateTime, default=datetime.utcnow)

    return PostLike


def create_user_post(db):
    class Posts(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        title = db.Column(db.String(255))
        content = db.Column(db.Text)
        slug = db.Column(db.String(255))
        date_posted = db.Column(db.DateTime, default=datetime.utcnow)
        post_id = db.Column(db.Integer, db.ForeignKey('users.id'))
        likes = db.relationship('PostLike', backref='post')
    return Posts
